package com.example.demobasicauthwithrole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBasicAuthWithRoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoBasicAuthWithRoleApplication.class, args);
    }

}
