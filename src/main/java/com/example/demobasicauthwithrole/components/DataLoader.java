package com.example.demobasicauthwithrole.components;


import com.example.demobasicauthwithrole.model.User;
import com.example.demobasicauthwithrole.model.UserRole;
import com.example.demobasicauthwithrole.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;


@Component
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Value("${admin.login}")
    private String login;

    @Value("${admin.password}")
    private String password;

    public DataLoader(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        Optional<User> byUsername = userRepository.findByUsername(login);
        if (!byUsername.isPresent()){
            userRepository.save(new User(
                    login,
                    passwordEncoder.encode(password),
                    Collections.singletonList(UserRole.ADMIN)
            ));
        }



    }
}
