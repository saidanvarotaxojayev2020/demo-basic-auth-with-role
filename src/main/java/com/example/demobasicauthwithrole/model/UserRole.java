package com.example.demobasicauthwithrole.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.security.core.GrantedAuthority;


public enum UserRole implements GrantedAuthority {
    ADMIN("ADMIN"),
    USER("USER");

    private String value;

    private UserRole(String value) {
        this.value = value;
    }

    @JsonCreator
    public static UserRole fromValue(String key) {
        if (key != null) {
            UserRole[] var1 = values();
            int var2 = var1.length;

            for (int var3 = 0; var3 < var2; ++var3) {
                UserRole type = var1[var3];
                if (type.value.equals(key)) {
                    return type;
                }
            }
        }

        return null;
    }

    @JsonValue
    public String toValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }

    public String getAuthority() {
        return this.value;
    }
}
