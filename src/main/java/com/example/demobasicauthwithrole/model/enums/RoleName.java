package com.example.demobasicauthwithrole.model.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_CLIENT,
}
