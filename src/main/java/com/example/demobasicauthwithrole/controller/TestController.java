package com.example.demobasicauthwithrole.controller;

import com.example.demobasicauthwithrole.model.User;
import com.example.demobasicauthwithrole.model.UserRole;
import com.example.demobasicauthwithrole.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RequiredArgsConstructor
@RestController
@RequestMapping("/test")
public class TestController {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/{word}")
    public String testWord(@PathVariable String word){
        return word;
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PostMapping("/add")
    public User addUser(@RequestBody User user){
        User user1 = user;
        user1.setPassword(passwordEncoder.encode(user.getPassword()));
        user1.setRoles(Collections.singletonList(UserRole.USER));
        User save = userRepository.save(user1);
        return save;
    }

}
